import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
// import { OrdersModule } from './orders/orders.module';
import { TypeOrmModule } from '@nestjs/typeorm';
// import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
// import { Order } from './orders/entities/order.entity';
// import { OrderItem } from './orders/entities/orderItem.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
// import { Member } from './member/entities/member.entity';
// import { MemberModule } from './member/member.module';
import { ExpenseModule } from './expense/expense.module';
import { Expense } from './expense/entities/expense.entity';
import { User } from './users/entities/user.entity';
import { Employee } from './employees/entities/employee.entity';
import { EmployeeModule } from './employees/employee.module';
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        User,
        Role,
        Type,
        Product,
        Expense,
        Employee,
        // Order,
        // OrderItem,
        Expense,
      ],
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    UsersModule,
    AuthModule,
    ProductsModule,
    // MemberModule,
    EmployeeModule,
    ExpenseModule,
    // OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
