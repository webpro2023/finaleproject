import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { Type } from './entities/type.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TypesService {
  constructor(
    @InjectRepository(Type) private typesRepository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typesRepository.save(createTypeDto);
  }

  findAll() {
    return this.typesRepository.find();
  }

  findOne(id: number) {
    return this.typesRepository.findOneBy({ id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typesRepository.findOneByOrFail({ id });
    await this.typesRepository.update(id, updateTypeDto);
    const updatedType = await this.typesRepository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const removedType = await this.typesRepository.findOneByOrFail({ id });
    await this.typesRepository.remove(removedType);
    return removedType;
  }
}
