export class CreateMemberDto {
  fullname: string;
  tel: string;
  email: string;
  password: string;
  point: string;
  birthday: string;
  userPoint: string;
}
