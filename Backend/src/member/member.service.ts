import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class MemberService {
  constructor(
    @InjectRepository(Member) private memberRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    createMemberDto.point = '0';
    createMemberDto.userPoint = '0';
    return this.memberRepository.save(createMemberDto);
  }

  findAll() {
    return this.memberRepository.find();
  }

  findOne(id: number) {
    return this.memberRepository.findOneBy({ id: id });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    // await this.memberRepository.findOneByOrFail({ id });
    await this.memberRepository.update(id, updateMemberDto);
    const updatedMember = await this.memberRepository.findOneBy({ id });
    return updatedMember;
  }

  async remove(id: number) {
    const removedMember = await this.memberRepository.findOneByOrFail({ id });
    await this.memberRepository.remove(removedMember);
    return removedMember;
  }
}
