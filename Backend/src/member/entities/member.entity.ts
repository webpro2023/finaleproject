import { Receipt } from 'src/receipts/entities/receipt.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  fullname: string;

  @Column()
  tel: string;

  @Column()
  email: string;
  @Column()
  password: string;

  @Column()
  point: string;

  @Column()
  birthday: string;

  @Column()
  usedPoint: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => Receipt, (receipt) => receipt.memberId)
  receipts: Receipt[];
}
