import { Type } from 'src/types/entities/type.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  totalBefore: number;

  @Column()
  memberDiscount: number;

  @Column()
  total: number;

  @Column({
    name: 'Amount',
    default: 0,
  })
  receivedAmount: number;

  @Column()
  change: number;

  @Column()
  paymentType: string;

  @Column({
    name: 'Qty',
    default: 0,
  })
  qty: number;

  @Column({
    name: 'MembersID',
    nullable: true,
    default: 0,
  })
  memberId: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => Type, (type) => type.products)
  type: Type;

  // @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  // orderItems: OrderItem[];
}
