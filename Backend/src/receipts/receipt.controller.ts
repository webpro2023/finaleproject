// /* eslint-disable prettier/prettier */
// import {
//   Controller,
//   Get,
//   Post,
//   Body,
//   Param,
//   Delete,
//   // UseGuards,
//   // UseInterceptors,
//   // UploadedFile,
//   Patch,
// } from '@nestjs/common';
// import { ReceiptsService } from './receipt.service';
// import { CreateReceiptDto } from './dto/create-receipt.dto';
// // import { UpdateReceiptDto } from './dto/update-receipt.dto';
// // import { AuthGuard } from 'src/auth/auth.guard';
// // import { FileInterceptor } from '@nestjs/platform-express';
// // import { diskStorage } from 'multer';
// // import { uuid } from 'uuidv4';
// // import { extname } from 'path';
// // @UseGuards(AuthGuard)
// @Controller('receipts')
// export class ReceiptsController {
//   constructor(private readonly receiptsService: ReceiptsService) {}

//   @Post()
//   create(@Body() createReceiptDto: CreateReceiptDto) {
//     return this.receiptsService.create(createReceiptDto);
//   }

//   @Get()
//   findAll() {
//     return this.receiptsService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.receiptsService.findOne(+id);
//   }
//   @Patch(':id')
//   update(@Param('id') id: string, @Body() UpdateReceiptDto: UpdateReceiptDto) {
//     return this.receiptsService.update(+id, UpdateReceiptDto);
//   }
//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.receiptsService.remove(+id);
//   }
// }
