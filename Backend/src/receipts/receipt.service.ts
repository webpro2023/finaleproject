// import { Injectable } from '@nestjs/common';
// import { CreateReceiptDto } from './dto/create-receipt.dto';
// import { UpdateReceiptDto } from './dto/update-receipt.dto';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Receipt } from './entities/receipt.entity';
// import { Repository } from 'typeorm';

// @Injectable()
// export class ReceiptsService {
//   constructor(
//     @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
//   ) {}
//   create(createReceiptDto: CreateReceiptDto) {
//     const receipt = new Receipt();
//     receipt.name = createReceiptDto.name;
//     receipt.price = parseFloat(createReceiptDto.price);
//     // receipt.type = JSON.parse(createReceiptDto.type);
//     // if (createReceiptDto.image && createReceiptDto.image !== '') {
//     //   receipt.image = createReceiptDto.image;
//     // }
//     return this.receiptsRepository.save(receipt);
//   }

//   findAll() {
//     return this.receiptsRepository.find();
//   }

//   findOne(id: number) {
//     return this.receiptsRepository.findOne({
//       where: { id },
//       relations: { type: true },
//     });
//   }

//   async update(id: number, updateReceiptDto: UpdateReceiptDto) {
//     const receipt = await this.receiptsRepository.findOneOrFail({
//       where: { id },
//     });
//     receipt.name = updateReceiptDto.name;
//     receipt.price = parseFloat(updateReceiptDto.price);
//     receipt.type = JSON.parse(updateReceiptDto.type);
//     if (updateReceiptDto.image && updateReceiptDto.image !== '') {
//       receipt.image = updateReceiptDto.image;
//     }
//     this.receiptsRepository.save(receipt);
//     const result = await this.receiptsRepository.findOne({
//       where: { id },
//       relations: { type: true },
//     });
//     return result;
//   }

//   async remove(id: number) {
//     const deleteReceipt = await this.receiptsRepository.findOneOrFail({
//       where: { id },
//     });
//     await this.receiptsRepository.remove(deleteReceipt);

//     return deleteReceipt;
//   }
// }
