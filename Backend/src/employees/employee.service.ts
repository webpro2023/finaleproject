import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    const employee = new Employee();
    employee.name = createEmployeeDto.name;
    employee.position = createEmployeeDto.position;
    employee.salary = createEmployeeDto.salary;
    if (createEmployeeDto.image && createEmployeeDto.image !== '') {
      employee.image = createEmployeeDto.image;
    }

    return this.employeeRepository.save(employee);
  }

  findAll() {
    return this.employeeRepository.find();
  }

  findOne(id: number) {
    return this.employeeRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = new Employee();
    employee.name = updateEmployeeDto.name;
    employee.position = updateEmployeeDto.position;
    employee.salary = updateEmployeeDto.salary;
    if (updateEmployeeDto.image && updateEmployeeDto.image !== '') {
      employee.image = updateEmployeeDto.image;
    }
    const updateEmployee = await this.employeeRepository.findOneOrFail({
      where: { id },
    });
    updateEmployee.name = employee.name;
    updateEmployee.position = employee.position;
    updateEmployee.salary = employee.salary;
    updateEmployee.image = employee.image;
    await this.employeeRepository.save(updateEmployee);
    const result = await this.employeeRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteEmployee = await this.employeeRepository.findOneOrFail({
      where: { id },
    });
    await this.employeeRepository.remove(deleteEmployee);

    return deleteEmployee;
  }
}
