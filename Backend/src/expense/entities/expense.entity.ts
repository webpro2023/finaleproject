import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Expense {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  waterUnit: number;

  @Column()
  waterPrice: number;

  @Column()
  elecUnit: number;

  @Column()
  elecPrice: number;

  @Column()
  totalElecPrice: number;

  @Column()
  totalWaterPrice: number;

  @Column()
  rent: number;

  @Column()
  total: number;

  @Column({ default: () => 'CURRENT_TIMESTAMP' })
  startDate: Date;
}
