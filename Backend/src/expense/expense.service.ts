import { Injectable } from '@nestjs/common';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { Expense } from './entities/expense.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class ExpenseService {
  constructor(
    @InjectRepository(Expense) private expenseRepository: Repository<Expense>,
  ) {}
  create(createTypeDto: CreateExpenseDto) {
    return this.expenseRepository.save(createTypeDto);
  }

  findAll() {
    return this.expenseRepository.find();
  }

  findOne(id: number) {
    return this.expenseRepository.findOneBy({ id });
  }

  async update(id: number, updateExpenseDto: UpdateExpenseDto) {
    await this.expenseRepository.findOneByOrFail({ id });
    await this.expenseRepository.update(id, updateExpenseDto);
    const updatedType = await this.expenseRepository.findOneBy({ id });
    return updatedType;
  }

  async remove(id: number) {
    const removedType = await this.expenseRepository.findOneByOrFail({ id });
    await this.expenseRepository.remove(removedType);
    return removedType;
  }
}
