import { defineStore } from 'pinia'

export const useHistoryCheckInCheckOut = defineStore('history', () => {
  type HistoryItem = {
    id: number
    checkInTimeStatus?: Date
    checkOutTimeStatus?: Date
  }

  const editHistoryCheckInCheckOut = new Array<HistoryItem>()

  function addToHistoryCheckInCheckOut(item: HistoryItem) {
    editHistoryCheckInCheckOut.push(item)
  }

  function clearEditHistoryCheckInCheckOut() {
    editHistoryCheckInCheckOut.length = 0
  }

  return {
    editHistoryCheckInCheckOut,
    addToHistoryCheckInCheckOut,
    clearEditHistoryCheckInCheckOut
  }
})
