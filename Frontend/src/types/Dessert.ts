type Dessert = {
  id: number
  name: string
  position: string
  checkIn: boolean
  checkOut: boolean
  createdate: Date
  checkInTime?: Date
  checkOutTime?: Date
  dessertItems?: DessertItem[]
  // Round: number
}

export type { Dessert }
