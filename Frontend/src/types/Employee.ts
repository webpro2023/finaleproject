type Employee = {
  id?: number
  name: string
  position: string
  salary: number
  image: string
}

export { type Employee }
