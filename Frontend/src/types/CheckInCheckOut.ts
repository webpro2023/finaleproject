type CheckInCheckOut = {
  id: number
  name: string
  position: string
  currentDate: Date
  checkIn: boolean
  checkOut: boolean
  checkInTimeStatus?: Date
  checkOutTimeStatus?: Date
}

export { type CheckInCheckOut }
