import type { Product } from './Product'

type ReceiptItem = {
  id: number
  name: string
  price: number
  unit: number
  productId: number
  product?: Product
  size: string
  level: string
  // round: number;
}

export { type ReceiptItem }
