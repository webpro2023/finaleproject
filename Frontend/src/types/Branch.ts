type Status = 'open' | 'close'
type Branch = {
  id: number
  address: String
  status: Status
}
export type { Branch }
