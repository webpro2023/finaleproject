type Checkbox = {
  id: number
  name: string
  position: string
  checkIn: boolean
  checkOut: boolean
  createdate: Date
  checkInTime?: Date
  checkOutTime?: Date
}

export { type Checkbox }
