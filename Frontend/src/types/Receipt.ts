import type { Member } from './Member'
import type { ReceiptItem } from './ReceiptItem'
import type { User } from './User'

type Receipt = {
  id: number
  createdDate: Date
  totalBefore: number
  memberDiscount: number
  total: number
  receivedAmount: number
  change: number
  paymentType: string
  qty: number
  userId: number
  user?: User
  memberId: number
  member?: Member
  receiptItems?: ReceiptItem[]
  // Round: number
}

export type { Receipt }
