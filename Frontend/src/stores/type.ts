import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import typeService from '@/service/type'
import type { Type } from '@/types/Type'

export const useTypeStore = defineStore('type', () => {
  const loadingStore = useLoadingStore()
  const types = ref<Type[]>([])
  const initialType: Type = {
    name: ''
  }
  const editedType = ref<Type>(JSON.parse(JSON.stringify(initialType)))

  async function getType(id: number) {
    loadingStore.doLoad()
    const res = await typeService.getType(id)
    editedType.value = res.data
    loadingStore.finish()
  }
  async function getTypes() {
    try {
      loadingStore.doLoad()
      const res = await typeService.getTypes()
      console.log(res.data)
      types.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveType() {
    loadingStore.doLoad()
    const type = editedType.value
    if (!type.id) {
      // Add new
      console.log('Post ' + JSON.stringify(type))
      const res = await typeService.addType(type)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(type))
      const res = await typeService.updateType(type)
    }

    await getTypes()
    loadingStore.finish()
  }
  async function deleteType() {
    loadingStore.doLoad()
    const type = editedType.value
    const res = await typeService.delType(type)

    await getTypes()
    loadingStore.finish()
  }

  function clearForm() {
    editedType.value = JSON.parse(JSON.stringify(initialType))
  }
  return { types, getTypes, saveType, deleteType, editedType, getType, clearForm }
})
