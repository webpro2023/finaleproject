import { ref, computed, type Ref } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'
import type { User } from '@/types/User'

export const userReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()

  // สร้าง computed property เพื่อคำนวณส่วนลดสมาชิก
  const memberDiscount = computed(() => {
    return memberStore.currentMember && memberStore.currentMember.name !== 'ไม่พบ' ? -10 : 0
  })

  const receiptDialog = ref(false)
  const currentUser = authStore.getCurrentUser()

  // กำหนดค่า userId โดยใช้ Nullish Coalescing Operator (??) เพื่อกำหนดค่าเริ่มต้นเป็น 0 หาก currentUser?.id เป็น undefined
  const userId = currentUser?.id ?? 0

  // แปลงค่า currentUser ให้เป็น undefined หากเป็น null
  const user: User | undefined = currentUser || undefined

  // ระบุชนิดของตัวแปร 'receipt' เป็น 'Ref<Receipt>'
  const receipt: Ref<Receipt> = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    totalBefore: 0,
    memberDiscount: 0,
    total: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: 'cash',
    userId: userId,
    user: user,
    memberId: 0,
    qty: 0 // เพิ่ม qty ไปยังอ็อบเจกต์ Receipt
  })

  const receiptItems = ref<ReceiptItem[]>([])

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: product.id,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product,
        size: '',
        level: ''
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }

  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }

  function calReceipt() {
    let totalBefore = 0
    for (const item of receiptItems.value) {
      totalBefore += item.price * item.unit
    }
    receipt.value.totalBefore = totalBefore
    receipt.value.total = totalBefore + receipt.value.memberDiscount
  }

  function showReceiptDialog() {
    receipt.value.receiptItems = receiptItems.value
    receiptDialog.value = true
  }

  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      totalBefore: 0,
      memberDiscount: 0,
      total: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: 'cash',
      userId: userId,
      user: user,
      memberId: 0,
      qty: 0 // เพิ่ม qty ไปยังอ็อบเจกต์ Receipt
    }

    memberStore.clear()
  }

  return {
    receiptItems,
    receipt,
    receiptDialog,
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    calReceipt,
    showReceiptDialog,
    clear
  }
})
