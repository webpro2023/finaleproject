import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import employeeService from '@/service/employee'
import type { Employee } from '@/types/Employee'
import { useMessageStore } from './message'

export const useEmployeeStore = defineStore('employee', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const employees = ref<Employee[]>([])
  const initialEmployee: Employee & { files: File[] } = {
    name: '',
    position: '',
    salary: 0,
    image: 'noimage.jpg',
    files: []
  }
  const editedEmployee = ref<Employee & { files: File[] }>(
    JSON.parse(JSON.stringify(initialEmployee))
  )

  async function getEmployee(id: number) {
    loadingStore.doLoad()
    const res = await employeeService.getEmployee(id)
    editedEmployee.value = res.data
    loadingStore.finish()
  }
  async function getEmployees() {
    try {
      loadingStore.doLoad()
      const res = await employeeService.getEmployees()
      employees.value = res.data
      loadingStore.finish()
    } catch (e) {
      loadingStore.finish()
    }
  }
  async function saveEmployee() {
    try {
      loadingStore.doLoad()
      const employee = editedEmployee.value
      if (!employee.id) {
        // Add new
        console.log('Post ' + JSON.stringify(employee))
        const res = await employeeService.addEmployee(employee)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(employee))
        const res = await employeeService.updateEmployee(employee)
      }

      await getEmployees()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteEmployee() {
    loadingStore.doLoad()
    const employee = editedEmployee.value
    const res = await employeeService.delEmployee(employee)

    await getEmployees()
    loadingStore.finish()
  }

  function clearForm() {
    editedEmployee.value = JSON.parse(JSON.stringify(initialEmployee))
  }
  return {
    employees,
    getEmployees,
    saveEmployee,
    deleteEmployee,
    editedEmployee,
    getEmployee,
    clearForm
  }
})
