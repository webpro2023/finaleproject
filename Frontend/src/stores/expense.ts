import type { Expense } from '@/types/Expense'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import expenseService from '@/service/expense'

export const useExpenseStore = defineStore('expense', () => {
  // const loadingStore = useLoadingStore()
  const expenses = ref<Expense[]>([])
  const receiptCostDialog = ref(false)
  const initialExpense: Expense = {
    waterUnit: 0,
    waterPrice: 0,
    elecUnit: 0,
    elecPrice: 0,
    totalElecPrice: 0,
    totalWaterPrice: 0,
    rent: 0,
    total: 0,

    startDate: new Date()
  }
  function calExpence() {
    editedExpense.value.totalElecPrice = editedExpense.value.elecUnit * 5
    editedExpense.value.totalWaterPrice = editedExpense.value.waterUnit * 7
    editedExpense.value.total =
      Number(editedExpense.value.totalElecPrice) +
      Number(editedExpense.value.totalWaterPrice) +
      Number(editedExpense.value.rent)
    console.log('elecUnit :' + editedExpense.value!.elecUnit)
    console.log('totaleleprice :' + editedExpense.value.totalElecPrice)
    console.log('waterUnit :' + editedExpense.value!.waterUnit)
    console.log('totalwaterprice :' + editedExpense.value.totalWaterPrice)
    console.log('rent :' + editedExpense.value.rent)
    console.log('total :' + editedExpense.value.total)
    console.log('Date :' + editedExpense.value.startDate)
  }

  const editedExpense = ref<Expense>(JSON.parse(JSON.stringify(initialExpense)))

  function showReceiptCostDialog() {
    receiptCostDialog.value = true
  }

  async function getExpense(id: number) {
    // loadingStore.doLoad()
    const res = await expenseService.getExpense(id)
    editedExpense.value = res.data
    // loadingStore.finish()
  }
  async function getExpenses() {
    try {
      // loadingStore.doLoad()
      const res = await expenseService.getExpenses()
      expenses.value = res.data
      // loadingStore.finish()
    } catch (e) {
      // loadingStore.finish()
    }
  }
  async function saveExpense() {
    // loadingStore.doLoad()
    const expense = editedExpense.value
    if (!expense.id) {
      // Add new
      console.log('Post ' + JSON.stringify(expense))
      const res = await expenseService.addExpense(expense)
    } else {
      // Update
      console.log('Patch ' + JSON.stringify(expense))
      const res = await expenseService.updateExpense(expense)
    }

    await getExpenses()
    // loadingStore.finish()
  }
  async function deleteExpense() {
    // loadingStore.doLoad()
    const expense = editedExpense.value
    const res = await expenseService.delExpense(expense)

    await getExpenses()
    // loadingStore.finish()
  }

  function clearForm() {
    editedExpense.value = JSON.parse(JSON.stringify(initialExpense))
  }
  return {
    expenses,
    initialExpense,
    receiptCostDialog,
    editedExpense,
    getExpenses,
    saveExpense,
    deleteExpense,
    getExpense,
    clearForm,
    calExpence,
    showReceiptCostDialog
  }
})
