import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Branch } from '@/types/Branch'
import { useLoadStore } from './loading'
import branchService from '@/service/branch'
export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadStore()
  const branchs = ref<Branch[]>([])
  // Data function
  async function getBranch(id: number) {
    // loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    branchs.value = res.data
    // loadingStore.finish()
  }
  async function getBranchs() {
    // loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    // loadingStore.finish()
  }
  async function saveBranch(branch: Branch) {
    // loadingStore.doLoad()
    if (branch.id < 0) {
      //add new
      console.log('Post ' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      //update
      console.log('Patch ' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }
    await getBranchs()
    // loadingStore.finish()
  }

  async function deleteBranch(branch: Branch) {
    // loadingStore.doLoad()
    const res = await branchService.delBranch(branch)
    await getBranchs()
    // loadingStore.finish()
  }

  return { getBranch, getBranchs, saveBranch, deleteBranch, branchs }
})
