import { ref } from 'vue'
import { defineStore } from 'pinia'

import type { Dessert } from '@/types/Dessert'
import type { Checkbox } from '@/types/Checkbox'
import type { DessertItem } from '@/types/DessertItem'

export const checkstatus = defineStore('status', () => {
  // let round = 1;

  const status = ref<Dessert>({
    id: 0,
    name: 'ว่าง',
    position: 'ว่าง',
    checkIn: false,
    checkOut: false,
    createdate: new Date(),
    checkInTime: new Date(),
    checkOutTime: new Date()
  })
  const dessertItems = ref<DessertItem[]>([])
  function addcheck(checkbox: Checkbox) {
    // let Round = 1
    const index = dessertItems.value.findIndex((item) => item.id == checkbox.id)
    // console.log(index)
    if (index >= 0) {
      dessertItems.value[index].checkIn = checkbox.checkIn
      dessertItems.value[index].checkOut = checkbox.checkOut

      return
    }
    const newStstus: DessertItem = {
      id: checkbox.id,
      name: checkbox.name,
      position: checkbox.position,
      checkIn: checkbox.checkIn,
      checkOut: checkbox.checkOut,
      createdate: checkbox.createdate,
      checkInTime: checkbox.checkInTime,
      checkOutTime: checkbox.checkOutTime
      // round: Round++
    }
    dessertItems.value.push(newStstus)
  }
  function clearstatus() {
    dessertItems.value = []
  }
  return {
    status,
    dessertItems,
    addcheck,
    clearstatus
  }
})
