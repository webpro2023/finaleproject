import { defineStore } from 'pinia'

// ตัวอย่างใน Pinia store
export const useLayoutStore = defineStore('layout', {
  state: () => ({
    showMenu: true // true หมายถึงแสดงเมนู, false หมายถึงซ่อนเมนู
  }),
  actions: {
    toggleMenu() {
      this.showMenu = !this.showMenu
    }
  }
})
