import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product) {
  const formData = new URLSearchParams()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', JSON.stringify(product.type))
  //formData.append('subType', product.subType.toString())
  //formData.append('size', product.size.toString())
  //formData.append('sweetLevel', product.sweetLevel.toString())
  // formData.append('file', product.files[0])
  // console.log(product.files)
  // if (product.files && product.files.length > 0) formData.append('file', product.files[0])
  return http.post(
    '/products',
    formData
    //   {
    //     headers: {
    //       'Content.Type': 'multipart/form-data'
    //     }
    //   }
  )
}
// & { files: File[]
function updateProduct(product: Product) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', JSON.stringify(product.type))
  //   formData.append('subType', product.subType.toString())
  //   formData.append('size', product.size.toString())
  //   formData.append('sweetLevel', product.sweetLevel.toString())
  // formData.append('file', product.files[0])
  // console.log(product.files)
  //   if (product.files && product.files.length > 0) formData.append('file', product.files[0])
  //   return http.post(`/products/${product.id}`, formData
  //   , {
  //     headers: {
  //       'Content.Type': 'multipart/form-data'
  //     }
  //   }
  //   )

  return http.patch(`/products/${product.id}`, product)
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}

function getProduct(id: number) {
  return http.get(`/products/${id}`)
}

function getProductsByType(typeId: number) {
  return http.get('/products/type/' + typeId)
}

function getProducts() {
  return http.get('/products')
}

export default { addProduct, updateProduct, delProduct, getProduct, getProducts, getProductsByType }
