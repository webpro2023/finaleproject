import type { Branch } from '../types/Branch'
import http from './http'

function addBranch(branch: Branch) {
  return http.post('/branch', branch)
}
function updateBranch(branch: Branch) {
  return http.patch(`/branch/${branch.id}`, branch)
}
function delBranch(branch: Branch) {
  return http.delete(`/branch/${branch.id}`)
}
function getBranch(id: number) {
  return http.delete(`/branch/${id}`)
}
function getBranchs() {
  return http.get('/branch')
}

export default { addBranch, updateBranch, delBranch, getBranch, getBranchs }
