import type { Employee } from '@/types/Employee'
import http from './http'

function addEmployee(employee: Employee & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', employee.name)
  formData.append('position', employee.position)
  formData.append('salary', employee.salary.toString())
  if (employee.files && employee.files.length > 0) {
    formData.append('file', employee.files[0])
  }
  return http.post('/employee', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateEmployee(employee: Employee & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', employee.name)
  formData.append('position', employee.position)
  formData.append('salary', employee.salary.toString())
  if (employee.files && employee.files.length > 0) {
    formData.append('file', employee.files[0])
  }
  return http.post(`/employee/${employee.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delEmployee(employee: Employee) {
  return http.delete(`/employee/${employee.id}`)
}

function getEmployee(id: number) {
  return http.get(`/employee/${id}`)
}

function getEmployees() {
  return http.get('/employee')
}

export default { addEmployee, updateEmployee, delEmployee, getEmployee, getEmployees }
